var mongoose = require('mongoose');
var User= require('./user');
var Schema = mongoose.Schema;

var WeivingSchema = new Schema({
    userId: { type: String, trim: true, required: false, lowercase: true },
    users:[{
        userId:{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }
    }]
});

WeivingSchema.static('findOrCreate', function (profile, callback) {
    var self = this;
    this.findOne({userId: profile._id},
        function (err, weiving) {
            if (err) return callback(err);
            if (weiving)
            {
                return callback(null, weiving);
            }
            weiving = new self({userId:profile._id,users:[]});
            weiving.save(callback);
        });
});

module.exports = mongoose.model('Weiving', WeivingSchema);