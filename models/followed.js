var mongoose = require('mongoose');
var User= require('./user');

var Schema = mongoose.Schema;

var FollowedSchema = new Schema({
    userId: { type: String, trim: true, required: false, lowercase: true },
    users:[{
        userId:{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }
    }]
});

FollowedSchema.static('findOrCreate', function (profile, callback) {
    var self = this;
    this.findOne({userId: profile._id},
        function (err, followed) {
            if (err) return callback(err);
            if (followed)
            {
                return callback(null, followed);
            }
            followed = new self({userId:profile._id,users:[]});
            followed.save(callback);
        });
});

module.exports = mongoose.model('Followed', FollowedSchema);