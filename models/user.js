/**
 * Created by nemanja on 8/10/16.
 */

var mongoose = require('mongoose');
var config = require('../config');
var algoliasearch = require('algoliasearch');
var client = algoliasearch("GLNG8ESMBL", "739ec341d50c9c50685f97f5e3535c5a");
var index = client.initIndex('UserIndex');

var Schema = mongoose.Schema;

var UserSchema = new Schema({
    fullName: { type: String , text: true},
    email: { type: String, trim: true, required: false, lowercase: true },
    username: { type: String, trim: true, required: false, lowercase: true , text: true},
    facebookId: { type: String, trim: true },
    googleId: { type: String, trim: true },
    instagramHandle: { type: String, trim: true },
    confirmed: { type: Boolean, default: false },
    deleted: { type: Boolean, default: false },
    joined_on: { type: Date },
    last_login_tried_on: { type: Date },
    is_online: { type: Boolean, default: false },
    is_online_last_date_time: { type: Date, default: Date.now() },
    finishedCreation: { type: Boolean, default: false },
    avatarUrl: { type: String },
    heroUrl: { type: String },
    gender: { type: String },
    description: { type: String },
    birthYear: { type: Number },
    is_admin: { type: Boolean, default: false },
    is_content_creator: { type: Boolean, default: false },
    subscription_plan:{type:Number},
    content_creator_stripe_id:{ type: String },
    socketId : { type: String, default: '' },
    bought: [{
        album: { type: mongoose.Schema.Types.ObjectId, ref: 'Album' },
        transaction: { type: String },
        date: { type: Date }
    }],
    subscribed:[{
        user:{type:mongoose.Schema.Types.ObjectId,ref:'User'},
        date:{type:Date}
    }],
    subscribedTo:[{
        user:{type:mongoose.Schema.Types.ObjectId,ref:'User'},
        receipt:{type:String, select: false},
        active:{type:Boolean},
        transaction:{type:mongoose.Schema.Types.ObjectId,ref:'Order'},
        date:{type:Date}
    }],
    fcmList:[{fcmToken:{type:String}}],
    activities:[{ type: mongoose.Schema.Types.ObjectId, ref: 'Activity' }]
    
});

UserSchema.pre('save', function (next) {
    this.wasNew = this.isNew;
    next();
});

UserSchema.post('save', function (doc) {
    if(this.confirmed && doc._id)
    {
        index.addObject(doc, doc._id.toString(), function (err, content) {
            console.log(content);
        });
    }
});

// UserSchema.post('update', function () {
//     var self = this;
//     var uid = self._conditions._id;
//     self.findOne({ _id: uid }, function (err, doc) {
//         if (err)
//             console.log(err);
//         else {
//             var suid = uid.toString();
//             doc.objectID = suid;
//             if(doc.confirmed)
//             {
//             index.saveObject(doc,doc._id.toString(), function (err, content) {
//                 console.log(content);
//             });
//             }
//         }
//     });
// });

UserSchema.static('findOrCreate', function (profile, callback) {
    var self = this;
    if(profile.facebookId)
    {
    this.findOne({facebookId: profile.facebookId},
        function (err, user) {
            if (err) return callback(err);
            if (user)
            {
                return callback(null, user);
            }
            user = new self({facebookId:profile.facebookId});
            user.joined_on = Date.now();
            user.save(callback);
        });
    }
    else if(profile.googleId)
    {
     this.findOne({googleId: profile.googleId},
        function (err, user) {
            if (err) return callback(err);
            if (user)
            {
                return callback(null, user);
            }
            user = new self({googleId:profile.googleId});
            user.joined_on = Date.now();
            user.save(callback);
        });   
    }
});

UserSchema.methods.outputSearchResult = function() {
    var $this = this;

    return {
        id: $this._id,
        fullName: $this.fullName,
        username: $this.username,
        is_online: $this.is_online,
        avatarUrl: $this.avatarUrl,
        heroUrl: $this.heroUrl,
        gender: $this.gender,
        description: $this.description,
        birthYear: $this.birthYear
    };
};

UserSchema.methods.outputChatResult = function(lastMessage, isRead) {
    var $this = this;

    return {
        id: $this._id,
        fullName: $this.fullName,
        username: $this.username,
        is_online: $this.is_online,
        avatarUrl: $this.avatarUrl,
        heroUrl: $this.heroUrl,
        gender: $this.gender,
        description: $this.description,
        birthYear: $this.birthYear,
        lastMessage: lastMessage,
        isRead: isRead
    };
};

UserSchema.methods.outputSellerPublicMappedResult = function(userLookingSellersProfile, albums) {
    var $this = this;

    
    //Avg rating for Seller based on all his Albums avg rating
    // var avgSellerRating = 0;
    // for(var i = 0; i < albums_output.length; i++) {
    //     avgSellerRating += albums_output[i].rating || 0;
    // }
    // avgSellerRating = avgSellerRating / albums_output.length;

    //if (userLookingSellersProfile == undefined || userLookingSellersProfile == null) {
        // Probably guest user is looking at seller profile, return only relevant seller data
        return {
            id: $this._id,
            fullName: $this.fullName,
            username: $this.username,
            is_online: $this.is_online,
            avatarUrl: $this.avatarUrl,
            heroUrl: $this.heroUrl,
            gender: $this.gender,
            description: $this.description,
            birthYear: $this.birthYear,
            albums: albums,
            confirmed:$this.confirmed
        };
    //}
};

module.exports = mongoose.model('User', UserSchema);