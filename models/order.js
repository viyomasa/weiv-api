/**
 * Created by nemanja on 5/21/17.
 */

var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var OrderSchema = new Schema({
    album: { type: mongoose.Schema.Types.ObjectId, ref: 'Album' },
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    transaction: { type: String },
    price: { type: Number },
    from_stripe: { type: String },
    to_stripe: { type: String },
    date: { type: Date }
});

module.exports = mongoose.model('Order', OrderSchema);
