var mongoose = require('mongoose');
var User= require('./user'); 

var Schema = mongoose.Schema;

var WeivedSchema = new Schema({
    userId: { type: String, trim: true, required: false, lowercase: true },
    users:[{
        userId:{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }
    }]
});

WeivedSchema.static('findOrCreate', function (profile, callback) {
    var self = this;
    this.findOne({userId: profile._id},
        function (err, weived) {
            if (err) return callback(err);
            if (weived)
            {
                return callback(null, weived);
            }
            weived = new self({userId:profile._id,users:[]});
            weived.save(callback);
        });
});

module.exports = mongoose.model('Weived', WeivedSchema);