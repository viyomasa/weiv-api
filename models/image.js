
var mongoose = require('mongoose');
var config   = require('../config');

var Schema = mongoose.Schema;

var ImageSchema = new Schema({
    tags: [{ type: String }],
    imageUserCreator: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    imageUrl: { type: String },
    videoThumb: { type: String },
    videoDuration: { type:String },
    description: { type: String },
    orderNumber: { type: Number, default: 0 },
    created_on: { type: Date, default: Date.now() },
    isCover: { type: Boolean, default: false },
    albumId: { type: String },
    type: { type: String, default: 'Image' }
});

ImageSchema.methods.toJSON = function() {
 var $this = this;

    if ($this.type == 'Video') {
        return {
            imageId: $this._id,
            imageUrl: config.defaultImageUrlPath + $this.imageUrl,
            imageThumbUrl50: $this.videoThumb,
            imageThumbUrl150: $this.videoThumb,
            imageThumbUrl300: $this.videoThumb,
            imageThumbUrl500: $this.videoThumb,
            tags: $this.tags,
            videoDuration:$this.videoDuration,
            orderNumber: $this.orderNumber,
            isCover: $this.isCover,
            albumId: $this.albumId,
            description: $this.description,
            type: $this.type
        };
    } else {
        return {
            imageId: $this._id,
            imageUrl: config.defaultImageUrlPath + $this.imageUrl,
            imageThumbUrl50: config.defaultImageUrlPath + "50x50/" + $this.imageUrl,
            imageThumbUrl150: config.defaultImageUrlPath + "150x150/" + $this.imageUrl,
            imageThumbUrl300: config.defaultImageUrlPath + "300x300/" + $this.imageUrl,
            imageThumbUrl500: config.defaultImageUrlPath + "500x500/" + $this.imageUrl,
            tags: $this.tags,
            orderNumber: $this.orderNumber,
            isCover: $this.isCover,
            albumId: $this.albumId,
            description: $this.description,
            type: $this.type
        }}
}

ImageSchema.methods.outputMappedResult = function() {
    var $this = this;

    if ($this.type == 'Video') {
        return {
            imageId: $this._id,
            imageUrl: config.defaultImageUrlPath + $this.imageUrl,
            imageThumbUrl50: $this.videoThumb,
            imageThumbUrl150: $this.videoThumb,
            imageThumbUrl300: $this.videoThumb,
            imageThumbUrl500: $this.videoThumb,
            tags: $this.tags,
            videoDuration:$this.videoDuration,
            orderNumber: $this.orderNumber,
            isCover: $this.isCover,
            albumId: $this.albumId,
            description: $this.description,
            type: $this.type
        };
    } else {
        return {
            imageId: $this._id,
            imageUrl: config.defaultImageUrlPath + $this.imageUrl,
            imageThumbUrl50: config.defaultImageUrlPath + "50x50/" + $this.imageUrl,
            imageThumbUrl150: config.defaultImageUrlPath + "150x150/" + $this.imageUrl,
            imageThumbUrl300: config.defaultImageUrlPath + "300x300/" + $this.imageUrl,
            imageThumbUrl500: config.defaultImageUrlPath + "500x500/" + $this.imageUrl,
            tags: $this.tags,
            orderNumber: $this.orderNumber,
            isCover: $this.isCover,
            albumId: $this.albumId,
            description: $this.description,
            type: $this.type
        };
    }
};

module.exports = mongoose.model('Image', ImageSchema);
