/**
 * Created by nemanja on 6/3/17.
 */
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var ActivitySchema = new Schema({
   message: { type: String },
   subject:{user:{ type: mongoose.Schema.Types.ObjectId, ref: 'User' },album:{ type: mongoose.Schema.Types.ObjectId, ref: 'Album' }},
   objecta:{user:{ type: mongoose.Schema.Types.ObjectId, ref: 'User' },album:{ type: mongoose.Schema.Types.ObjectId, ref: 'Album' }},
   activityType:{ type: String },
   created_on: { type: Date, default: Date.now() }
});

ActivitySchema.methods.outputMappedResult = function() {
    var $this = this;

    return {
        message:$this.message,
        subject:$this.subject,
        objecta:$this.objecta,
        status:$this.status,
        activityType:$this.activityType
    };
};

module.exports = mongoose.model('Activity', ActivitySchema);