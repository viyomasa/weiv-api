/**
 * Created by nemanja on 6/3/17.
 */
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var BasePostObjectSchema = new Schema({
   postType: { type: String },
   orderNum:{type:Number}
});

BasePostObjectSchema.methods.outputMappedResult = function() {
    var $this = this;
    return {
        postType:$this.postType,
        orderNum:$this.orderNum
    };
};

module.exports = mongoose.model('BasePostObject', BasePostObjectSchema);