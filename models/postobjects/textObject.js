/**
 * Created by nemanja on 6/3/17.
 */
var mongoose = require('mongoose');
var BasePostObject=require('./basePostObject');

var Schema = mongoose.Schema;

var TextObjectSchema = new Schema({
   text: { type: String },
   font:{
       size:{type:Number},
       family:{type:String}
   }
});

TextObjectSchema.methods.outputMappedResult = function() {
    var $this = this;
    return {
        type:$this.postType
    };
};

module.exports = BasePostObject.discriminator('TextObject', TextObjectSchema);