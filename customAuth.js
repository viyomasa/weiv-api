var passport = require("passport");
var passportJWT = require("passport-jwt");
var config = require("./config.js");
var ExtractJwt = passportJWT.ExtractJwt;
var Strategy = passportJWT.Strategy;
var User = require('./models/user');
var params = {
    secretOrKey: config.secret,
    jwtFromRequest: ExtractJwt.fromAuthHeader()
};

module.exports = function () {
    var strategy = new Strategy(params, function (payload, done) {
        User.findOne({ _id: payload.id }, function (err, user) {
            if (user) {
                return done(null, user);
            } else {
                return done(new Error("User not found"), null);
            }
        });

    });
    passport.use(strategy);
    return {
        initialize: function () {
            return passport.initialize();
        },
        authenticate: function () {
            return passport.authenticate("jwt", { session: false });
        }
    };
};