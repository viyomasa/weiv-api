// Required
var http                    = require('http');
var express                 = require('express');
var app                     = express();
var bodyParser              = require('body-parser');
var morgan                  = require('morgan');
var mongoose                = require('mongoose');
var config                  = require('./config');
// Models
var User                    = require('./models/user');
var Chat                    = require('./models/chat');
// Routes
var user_routes             = require('./routes/user-routes');
var image_routes            = require('./routes/image-routes');
var video_routes            = require('./routes/video-routes');
var album_routes            = require('./routes/album-routes');
var admin_routes            = require('./routes/admin-routes');
var stripe_webhooks_routes  = require('./routes/stripe-webhooks-routes');

var passport                = require('passport');
var auth                    = require("./customAuth")();
// Chat
var socketio                = require('socket.io');
var redis                   = require('redis');
var redisAdapter            = require('socket.io-redis');
var redisOptions            = require('parse-redis-url')(redis).parse(config.redis.fullUrl);
var handleErrorResponse     = require('./helpers/errorHelper').handleErrorResponse;
var getUserFromToken        = require('./helpers/user-from-token-helper').getUserFromToken;

var adminUser = {
  id : 'd63d36d76gaii92201q934jdaehkuh23dddu29',
  email : 'superadmin@weiv.com',
  password : 'g@dde43yyya!!6+7'
};


var session                 = require('express-session');
var flash                   = require('connect-flash');



var pub = redis.createClient(redisOptions.port, redisOptions.host, {
    auth_pass: redisOptions.password,
    socket_keepalive: true
});
var sub = redis.createClient(redisOptions.port, redisOptions.host, {
    return_buffers: true,
    auth_pass: redisOptions.password,
    socket_keepalive: true
});
sub.on('error', function (er) {
    console.trace('Redis SUB module error');
    console.error(er.stack);
});
pub.on('error', function (er) {
    console.trace('Redis PUB module error');
    console.error(er.stack);
});
sub.subscribe('eventMessaging');
sub.on('message', function (channel, message) {
    var result = JSON.parse(message);
    redisMessageDeliverOnSocket(result.eventName, result.socket_ids, result.data);
});

mongoose.connect(config.database);

app.use(morgan('dev'));
app.use(passport.initialize());
app.use(auth.initialize());
app.use(session({ secret: config.secret }));
app.use(flash());
app.use(bodyParser.json({limit: '100mb'}));
app.use(bodyParser.urlencoded({limit: '100mb', extended: true}));
app.set('view engine', 'ejs');
app.use('/node_modules/ng-admin/build', express.static(__dirname + '/node_modules/ng-admin/build'));
app.set('jwtTokenSecret', '1adwer2rtzzzG456');
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", config.origin);
    res.header("Access-Control-Allow-Credentials", "true");
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, x-access-token");
    next();
});

app.get('/', function(req, res) {
    res.send('Hello! AutoDeployed API - The API is at this URL: http://localhost:' + config.port + '/api');
});

app.use('/api/user', user_routes);
app.use('/api/image', image_routes);
app.use('/api/video', video_routes);
app.use('/api/album', album_routes);
app.use('/api/stripewebhooks', stripe_webhooks_routes);
app.use('/api_admin', admin_routes);
app.use('/static',express.static('static'));

app.get('/admin',isLoggedInAdmin, function(req, res) {
    res.render('admin_index');
});

app.get('/admin/login', function(req, res) {
  // render the page and pass in any flash data if it exists
  res.render('login.ejs', { message: req.flash('loginMessage') });
});

app.post('/admin/login', function(req, res) {
  if (adminUser.email === req.body.email && adminUser.password === req.body.password) {
    req.session.currentAdminUser = adminUser;
    res.redirect('/admin');
  } else {
    req.flash('loginMessage', 'Oops! Bad email and/or password!');
    res.redirect('/admin/login');
  }
});

app.get('/admin/logout', function(req, res) {
  delete req.session.currentAdminUser;
  res.redirect('/admin');
});

// route middleware to make sure a user is logged in
function isLoggedInAdmin(req, res, next) {

  // if user is authenticated in the session, carry on
  if (req.session && req.session.currentAdminUser && req.session.currentAdminUser.id == adminUser.id)
    return next();

  // if they aren't redirect them to the home page
  res.redirect('/admin/login');
}




app.get('/api/chat', auth.authenticate(), function(req, res) {
    var possibleErrors = [];

    User.findOne({_id : req.user._id }, function(err, userDecoded) {
        if (!userDecoded || err) {
            possibleErrors.push('unknown auth user!');
        }

        if (possibleErrors.length == 0) {
            Chat.find({ $or:[ { 'userSender.user': userDecoded.id }, { 'userReceiver.user': userDecoded.id }]}).populate('userSender.user userReceiver.user').sort({'created_on': -1}).exec(function(err, chats) {
                if (err) {
                    possibleErrors.push('Error on chat lookup!' + err.message);
                }
                var users = [];
                var usersIds = [];
                chats.forEach(function(chat) {
                    if (usersIds.indexOf(chat.userSender.user._id.toString()) < 0 && !chat.userSender.user._id.equals(req.user._id)) {
                        users.push(chat.userSender.user.outputChatResult(chat.message, chat.isRead));
                        usersIds.push(chat.userSender.user._id.toString());
                    }
                    if (usersIds.indexOf(chat.userReceiver.user._id.toString()) < 0 && !chat.userReceiver.user._id.equals(req.user._id)) {
                        users.push(chat.userReceiver.user.outputChatResult(chat.message, true));
                        usersIds.push(chat.userReceiver.user._id.toString());
                    }
                });

                if (possibleErrors.length == 0) {
                    return res.status(200).jsonp(users);
                } else {
                    return handleErrorResponse(possibleErrors, res);
                }
            });

        } else {
            return handleErrorResponse(possibleErrors, res);
        }

    });
});

app.get('/api/chat/:id', auth.authenticate(), function(req, res) {
    var possibleErrors = [];
    if (!req.params.id || req.params.id == 0) {
        possibleErrors.push('id is required!');
        return handleErrorResponse(possibleErrors, res);
    }

    User.findOne({_id : req.user._id }, function(err, userDecoded) {
        if (!userDecoded || err) {
            possibleErrors.push('unknown auth user!');
        }

        User.findOne({_id: req.params.id}, function (err, userRef) {
            if (err || !userRef) {
                possibleErrors.push('unknown targeted user');
            }

            if (possibleErrors.length == 0) {
                Chat.find({ $or:[ { $and: [{ 'userSender.user': userDecoded.id }, { 'userReceiver.user': userRef.id }] },
                    { $and: [{ 'userReceiver.user': userDecoded.id },{ 'userSender.user': userRef.id }] } ]}).sort({'created_on': -1}).limit(20).populate('userSender.user userReceiver.user').exec(function(err, chats) {
                    if (err) {
                        possibleErrors.push('Error on chat lookup!' + err.message);
                    }

                    if (possibleErrors.length == 0) {
                        return res.status(200).jsonp(chats.map(function(chat) { return chat.outputMappedResult(); }));
                    } else {
                        return handleErrorResponse(possibleErrors, res);
                    }
                });

            } else {
                return handleErrorResponse(possibleErrors, res);
            }
        });
    });
});

app.post('/api/chat/send/:id', auth.authenticate(), function(req, res) {
    var possibleErrors = [];
    if (!req.params.id || req.params.id == 0) {
        possibleErrors.push('id is required!');
        return handleErrorResponse(possibleErrors, res);
    }

    User.findOne({_id : req.user._id }, function(err, userDecoded) {
        if (!userDecoded || err) {
            possibleErrors.push('unknown auth user!');
        }

        User.findOne({_id: req.params.id}, function (err, userRef) {
            if (err || !userRef) {
                possibleErrors.push('unknown targeted user');
            }

            if (possibleErrors.length == 0) {

                var subChatSender = {
                    user: req.user._id
                };
                var subChatReceiver = {
                    user: req.params.id
                };
                var chat = new Chat();
                chat.userSender = subChatSender;
                chat.userReceiver = subChatReceiver;
                chat.message = req.body.message;
                chat.created_on = Date.now();

                chat.save(function (err) {
                    if (err) {
                        possibleErrors.push('unable to save chat message!');
                    }

                    if (possibleErrors.length == 0) {
                        socketNotify(
                            'newChatMessage',
                            [userDecoded.socketId, userRef.socketId],
                            {
                                userSenderId: userDecoded.id,
                                userReceiverId: userRef.id,
                                message : req.body.message
                            }
                        );

                        return res.status(200).jsonp({
                            status: 'sent'
                        });
                    } else {
                        return handleErrorResponse(possibleErrors, res);
                    }
                });

            } else {
                return handleErrorResponse(possibleErrors, res);
            }
        });
    });
});

app.post('/api/chat/read/:id', auth.authenticate(), function(req, res) {
    var possibleErrors = [];
    if (!req.params.id || req.params.id == 0) {
        possibleErrors.push('id is required!');
        return handleErrorResponse(possibleErrors, res);
    }

    User.findOne({_id : req.user._id }, function(err, userDecoded) {
        if (!userDecoded || err) {
            possibleErrors.push('unknown auth user!');
        }

        User.findOne({_id: req.params.id}, function (err, userRef) {
            if (err || !userRef) {
                possibleErrors.push('unknown targeted user');
            }

            if (possibleErrors.length == 0) {

                Chat.find({ $and: [{ 'userSender.user': userRef.id }, { 'userReceiver.user': userDecoded.id }] }).sort({'created_on': -1}).limit(20).exec(function(err, chats) {
                    if (err) {
                        possibleErrors.push('Error on chat lookup!' + err.message);
                    }

                    if (chats.length > 0 && chats[0].isRead == false)
                    {
                        chats[0].isRead = true;
                        chats[0].save();
                    }

                    if (possibleErrors.length == 0) {
                        return res.status(200).jsonp({ message: 'updated'});
                    } else {
                        return handleErrorResponse(possibleErrors, res);
                    }
                });


            } else {
                return handleErrorResponse(possibleErrors, res);
            }
        });
    });
});

app.set('port', config.port);
var server = http.createServer(app);

/**
 * handle socket connections
 */
var io = socketio(server);

io.adapter(redisAdapter({
    pubClient: pub,
    subClient: sub
}));

io.use(function(socket, next) {
    var token = socket.request._query.token;
    getUserFromToken(token, function(err, user){
        if (err || user == null) {
            console.trace('io.use error');
            console.log(err);
            console.log(user);
            next(new Error("not authorized"));
            return;
        }
        socket.user_id = user.id;
        next();
    });
});

io.on('connection', function(socket){
    subscribeUserToPublicEventListeners(socket.user_id, socket.id, function(err, message) {
        if (err) {
            console.error(err);
        } else {
            console.log('a auth user connected ' + socket.user_id + ' in pid ' + process.pid);
        }
    });
    socket.on('disconnect', function() {
        unsubscribeUserFromPublicEventListeners(socket.user_id, function(err, message) {
            if (err) {
                console.error(err);
            } else {
                console.log('auth user disconnected ' + socket.user_id);
            }
        });
    });
});

var SocketIO = io;
var RedisSub = sub;
var RedisPub = pub;

function redisMessageDeliverOnSocket(eventName, socket_ids, data) {
    console.log('1) redisMessageDeliverOnSocket started for event ' + eventName);
    if(SocketIO) {
        console.log('2) redisMessageDeliverOnSocket instance exists');
        socket_ids.forEach(function(socket_id) {
            if (socket_id != '' && SocketIO.sockets.connected[socket_id]) {
                console.log('3) socket exist in this PID now emmiting message to socketId: ' + socket_id);
                data.pid = process.pid;
                SocketIO.sockets.connected[socket_id].emit(eventName, data);
            }
        });
    }
};

function socketNotify(eventName, socket_ids, data) {
    //console.log('1) socketNotify started for event ' + eventName);
    if(RedisPub) {
        console.log('2) socketNotify instance exists');
        RedisPub.publish('eventMessaging', JSON.stringify(
            {
                eventName: eventName,
                socket_ids: socket_ids,
                data: data
            }));
    }

    // socket_ids.forEach(function(socket_id) {
    //     if (socket_id != '' && SocketIO.sockets.connected[socket_id]) {
    //         //console.log('3) socket exist in this PID now emmiting message to socketId: ' + socket_id);
    //         data.pid = process.pid;
    //         SocketIO.sockets.connected[socket_id].emit(eventName, data);
    //     }
    // });
};

function subscribeUserToPublicEventListeners(userId, socketId, callback) {
    User.findOne({ _id : userId }, function (err, user) {
        if (err){
            callback('Unable to find user by provided Id!', undefined);
            return;
        }

        user.socketId = socketId;
        user.is_online = true;
        user.is_online_last_date_time = Date.now();

        user.save(function (err) {
            if (err) {
                callback(err, "Not subscribed");
            }

            // NOT NEEDED IN THIS VERSION
            // Match.find({ $or:[ {'userLeft.user':userId}, {'userRight.user':userId} ]}).populate('user').exec(function(err, matches) {
            //     if (!err) {
            //         var matches_ids = matches.map(function (x) { return x.userLeft.user; }).concat(matches.map(function (x) { return x.userRight.user; }));
            //         User.find({ _id: {$in: matches_ids } },
            //             function (err, usersReturned) {
            //                 if (!err) {
            //                     var socket_ids = usersReturned.map(function (x) { return x.socketId });
            //                     socketNotify(
            //                         'matchIsOnOffline',
            //                         socket_ids,
            //                         user.outputMappedResult('socket', null)
            //                     );
            //                 }
            //             });
            //     }
            // });

            callback(null, "Subscribed");
        });
    });
};

function unsubscribeUserFromPublicEventListeners(userId, callback) {
    User.findOne({ _id: userId }, function (err, user) {
        if (err){
            callback('Unable to find user by provided Id!', undefined);
            return;
        }

        user.socket_id = '';
        user.is_online = false;

        user.save(function (err) {
            if (err) {
                callback(err, "Unsubscribed");
            }

            // NOT NEEDED IN THIS VERSION
            // Match.find({ $or:[ {'userLeft.user':userId}, {'userRight.user':userId} ]}).populate('user').exec(function(err, matches) {
            //     if (!err) {
            //         var matches_ids = matches.map(function (x) { return x.userLeft.user; }).concat(matches.map(function (x) { return x.userRight.user; }));
            //         User.find({ _id: {$in: matches_ids } },
            //             function (err, usersReturned) {
            //                 if (!err) {
            //                     var socket_ids = usersReturned.map(function (x) { return x.socketId });
            //                     socketNotify(
            //                         'matchIsOnOffline',
            //                         socket_ids,
            //                         user.outputMappedResult('socket', null)
            //                     );
            //                 }
            //             });
            //     }
            // });

            callback(null, "Unsubscribed");
        });
    });
};

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(config.port);
server.on('error', onError);
server.on('listening', onListening);

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

function onListening() {
    var addr = server.address();
    var bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;
    console.log('Listening on ' + bind + ' and PID:' + process.pid);
}
