/**
 * Created by nemanja on 5/21/17.
 */
const admin_routes = require('express').Router();

var User = require('../models/user');
var Album = require('../models/album');
var Image = require('../models/image');
var Order = require('../models/order');
var algoliaHelper=require('../helpers/algoliaHelper');
var notificationsHelper = require('../helpers/notificationsHelper');

var formatFilters = function(_filters) {
    var outputFilters = {};
    if (_filters) {
        for (var prop in _filters) {
            if(!_filters.hasOwnProperty(prop))
                continue;

            var propName = prop;
            if (prop.indexOf("_gte") > 0) {
                propName = prop.replace('_gte','');
                if (outputFilters[propName]) {
                    var tmp = outputFilters[propName];
                    tmp['$gte'] = _filters[prop];
                    outputFilters[propName] = tmp;
                } else {
                    outputFilters[propName] = {'$gte': _filters[prop]};
                }
            }
            else if (prop.indexOf("_lte") > 0) {
                propName = prop.replace('_lte','');

                if (outputFilters[propName]) {
                    var tmp = outputFilters[propName];
                    tmp['$lte'] = _filters[prop];
                    outputFilters[propName] = tmp;
                } else {
                    outputFilters[propName] = {'$lte': _filters[prop]};
                }
            }
            else if (prop.indexOf("_like") > 0) {
                propName = prop.replace('_like','');
                outputFilters[propName] = { '$regex' : _filters[prop], '$options' : 'i' };
            }
            else {
                outputFilters[propName] = _filters[prop];
            }
        }
    }
    return outputFilters;
};

// USER ADMIN ROUTES
admin_routes.get('/user', function(req, res, next) {
    var skip = 0;
    var limit = 1000000;
    var sortField = req.query._sortField;
    var sortDir = req.query._sortDir == "DESC" ? -1 : 1;
    var sortQuery = {};
    var filters = formatFilters(req.query._filters ? JSON.parse(req.query._filters) : {});
    sortQuery[sortField] = sortDir;

    if (req.query._perPage && Number(req.query._perPage) > 0 && sortField) {
        limit = Number(req.query._perPage);
    }
    if (req.query._page && Number(req.query._page) >= 0) {
        skip = (Number(req.query._page)-1) * limit;
    }
    User.count(filters, function(err, totalNumber) {
        User.find(filters).skip(skip).limit(limit).sort(sortQuery).exec(function(err, users) {
            if (err)
                res.send(err);

            res.setHeader('X-Total-Count', totalNumber);
            res.json(users);
        });
    });
});

admin_routes.get('/user/:id', function(req, res, next) {
    User.findById(req.params.id, function(err, user) {
        if (err)
            res.send(err);

        res.json(user);
    });
});

admin_routes.put('/user', function (req, res) {
    User.findOne({"_id": req.body._id}, function (err, user) {
            if (err)
                return res.send(err);

            if (user.is_content_creator && !user.deleted && user.finishedCreation) {
                user.confirmed = req.body.confirmed;
                user.save(function (err, us) {
                    if (err) {
                        return res.status(500).send(err)
                    }
                    if(req.body.confirmed)
                    notificationsHelper.sendNotifications("You have been approved as the WEIVmaker.","Create your first post now.",user.fcmList);
                    return res.json({message: 'Successfully updated'});
                });
            } else {
                return res.status(500).send(new Error('Unable to approve user!'));
            }
        });
});

// ALBUM ADMIN ROUTES
admin_routes.get('/album', function(req, res, next) {
    var skip = 0;
    var limit = 1000000;
    var sortField = req.query._sortField;
    var sortDir = req.query._sortDir == "DESC" ? -1 : 1;
    var sortQuery = {};
    var filters = formatFilters(req.query._filters ? JSON.parse(req.query._filters) : {});
    sortQuery[sortField] = sortDir;

    if (req.query._perPage && Number(req.query._perPage) > 0 && sortField) {
        limit = Number(req.query._perPage);
    }
    if (req.query._page && Number(req.query._page) >= 0) {
        skip = (Number(req.query._page)-1) * limit;
    }
    Album.count(filters, function(err, totalNumber) {
        Album.find(filters).skip(skip).limit(limit).sort(sortQuery).exec(function(err, albums) {
            if (err)
                res.send(err);

            var albumIds = albums.map(function (x) { return x._id; });
            var albumCreatorIds = albums.map(function (x) { return x.albumUserCreator; });
            User.find({ _id: albumCreatorIds }, function (err, creators) {
                Image.find({ albumId: { $in: albumIds } }, function (errI, images) {

                    var albums_output = albums.map(function (x) {
                        return x.outputMappedResultWithCreator(images, creators,null);
                    });

                    res.setHeader('X-Total-Count', totalNumber);
                    res.json(albums_output);
                });
            });
        });
    });
});

admin_routes.get('/album/:id', function(req, res, next) {
    Album.findById(req.params.id, function(err, album) {
        if (err)
            res.send(err);

        var albumIds = [req.params.id];
        var albumCreatorIds = [album.albumUserCreator];
        User.find({ _id: albumCreatorIds }, function (err, creators) {
            Image.find({ albumId: { $in: albumIds } }, function (errI, images) {
                var albums_output = album.outputMappedResultWithCreator(images, creators,null);
                res.json(albums_output);
            });
        });

    });
});

admin_routes.put('/album', function (req, res) {
    Album.findOne({"_id": req.body._id}).populate('albumUserCreator').exec(function (err, album) {
        if (err)
            return res.send(err);

        if (album.albumUserCreator.is_content_creator && !album.albumUserCreator.deleted && album.albumUserCreator.finishedCreation && album.albumUserCreator.confirmed && album.stage=='published') {
            album.isFeatured = req.body.isFeatured;
            album.isDiscoverable = req.body.isDiscoverable;
            album.save(function (err, al) {
                if (err) {
                    return res.status(500).send(err)
                }
                return res.json({message: 'Successfully updated'});
            });
        } else {
            return res.status(500).send(new Error('Unable to update album!'));
        }
    });
});

//ORDER ADMIN ROUTES
admin_routes.get('/order', function(req, res, next) {
    var skip = 0;
    var limit = 1000000;
    var sortField = req.query._sortField;
    var sortDir = req.query._sortDir == "DESC" ? -1 : 1;
    var sortQuery = {};
    var filters = formatFilters(req.query._filters ? JSON.parse(req.query._filters) : {});
    sortQuery[sortField] = sortDir;

    if (req.query._perPage && Number(req.query._perPage) > 0 && sortField) {
        limit = Number(req.query._perPage);
    }
    if (req.query._page && Number(req.query._page) >= 0) {
        skip = (Number(req.query._page)-1) * limit;
    }

    Order.count(filters, function(err, totalNumber) {
        Order.find(filters).populate('user album').skip(skip).limit(limit).sort(sortQuery).exec(function(err, orders) {
            if (err)
                res.send(err);

            res.setHeader('X-Total-Count', totalNumber);
            res.json(orders);
        });
    });
});

admin_routes.post('/algoliaSync', function(req, res, next) {
    if(req.body.token=="paralelopiped234acBBB")
    {
        algoliaHelper.syncUsers();
        algoliaHelper.syncAlbums();
        res.json("Synced")
    }
    else
    res.json("Not Authenticated")
});

module.exports = admin_routes;