var video_routes = require('express').Router();
var config  = require('../config');
var fs = require('fs');
var AWS = require('aws-sdk');
AWS.config.loadFromPath('./aws-config.json');
var s3 = new AWS.S3( { params: { Bucket: config.S3BucketCredentials.bucketName } } );

var uid = require('uid');
var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty();

var User = require('../models/user');
var Image = require('../models/image');
var handleErrorResponse = require('../helpers/errorHelper').handleErrorResponse;
var auth = require("../customAuth")();

video_routes.post('/', auth.authenticate(), multipartyMiddleware, function (req, res) {
    var possibleErrors = [];
    var file = req.files.video;
    var publicPrefixName = uid(20);
    var buffer = fs.readFileSync(file.path);

    var startTime = new Date();
    var partNum = 0;
    var partSize = 1024 * 1024 * 5; // 5mb chunks except last part
    var numPartsLeft = Math.ceil(buffer.length / partSize);
    var maxUploadTries = 3;

    var multipartParams = {
        Bucket: config.S3BucketCredentials.bucketName,
        Key: publicPrefixName,
        ContentType: file.type
    };

    var multipartMap = {
        Parts: []
    };

    console.log('Creating multipart upload for:', file.name);
    s3.createMultipartUpload(multipartParams, function(mpErr, multipart) {
        if (mpErr) return console.error('Error!', mpErr);
        console.log('Got upload ID', multipart.UploadId);

        for (var start = 0; start < buffer.length; start += partSize) {
            partNum++;
            var end = Math.min(start + partSize, buffer.length);
            var partParams = {
                Body: buffer.slice(start, end),
                Bucket: multipartParams.Bucket,
                Key: multipartParams.Key,
                PartNumber: String(partNum),
                UploadId: multipart.UploadId
            };

            console.log('Uploading part: #', partParams.PartNumber, ', Start:', start);
            uploadPart(s3, multipart, partParams);
        }
    });

    function completeMultipartUpload(s3, doneParams) {
        s3.completeMultipartUpload(doneParams, function(err, data) {
            if (err) return console.error('An error occurred while completing multipart upload');
            var delta = (new Date() - startTime) / 1000;
            console.log('Completed upload in', delta, 'seconds');
            console.log('Final upload data:', data);

            var newImage = new Image();
            newImage.description = req.body.description;
            newImage.tags = req.body.tags;
            newImage.orderNumber =  req.body.orderNumber;
            newImage.albumId =  req.body.albumId;
            newImage.isCover =  req.body.isCover;
            newImage.created_on = Date.now();
            newImage.imageUserCreator = req.user._id;
            newImage.imageUrl = publicPrefixName;
            newImage.videoThumb=req.body.videoThumb;
            newImage.videoDuration=req.body.videoDuration;
            newImage.type = 'Video';

            newImage.save(function (err) {
                if (err) {
                    possibleErrors.push('Video not saved!');
                }

                if (possibleErrors.length == 0) {
                    var output = newImage.outputMappedResult();
                    output.status = 'created';
                    return res.status(201).jsonp(output);
                } else {
                    return handleErrorResponse(possibleErrors, res);
                }
            });
        });
    }

    function uploadPart(s3, multipart, partParams, tryNum) {
        var tryNum = tryNum || 1;
        s3.uploadPart(partParams, function(multiErr, mData) {
            console.log('started');
            if (multiErr) {
                console.log('Upload part error:', multiErr);

                if (tryNum < maxUploadTries) {
                    console.log('Retrying upload of part: #', partParams.PartNumber);
                    uploadPart(s3, multipart, partParams, tryNum + 1);
                } else {
                    console.log('Failed uploading part: #', partParams.PartNumber);
                }
                // return;
            }

            multipartMap.Parts[this.request.params.PartNumber - 1] = {
                ETag: mData.ETag,
                PartNumber: Number(this.request.params.PartNumber)
            };
            console.log('Completed part', this.request.params.PartNumber);
            console.log('mData', mData);
            if (--numPartsLeft > 0) return; // complete only when all parts uploaded

            var doneParams = {
                Bucket: multipartParams.Bucket,
                Key: multipartParams.Key,
                MultipartUpload: multipartMap,
                UploadId: multipart.UploadId
            };

            console.log('Completing upload...');
            completeMultipartUpload(s3, doneParams);
        }).on('httpUploadProgress', function(progress) {  console.log(Math.round(progress.loaded/progress.total*100)+ '% done') });
    }

});

video_routes.get("/", function(httpRequest, httpResponse, next){
    httpResponse.send("<form action='http://sample-env.kgmapzcwd8.us-west-2.elasticbeanstalk.com/api/video/' method='post' enctype='multipart/form-data'><input type='file' name='video' /><input type='submit' value='Submit' /></form>");
});

module.exports = video_routes;