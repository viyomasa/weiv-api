
var image_routes = require('express').Router();
var config  = require('../config');
var fs = require('fs');
var AWS = require('aws-sdk');
AWS.config.loadFromPath('./aws-config.json');
var s3Bucket = new AWS.S3( { params: { Bucket: config.S3BucketCredentials.bucketName } } );

var uid = require('uid');

var User = require('../models/user');
var Image = require('../models/image');
var handleErrorResponse = require('../helpers/errorHelper').handleErrorResponse;
var auth = require("../customAuth")();

image_routes.post('/', auth.authenticate(), function(req, res) {
    var possibleErrors = [];

    if (!req.body.imageData || req.body.imageData.length == 0) {
        possibleErrors.push('imageData is required!');
    }
    if (!req.body.orderNumber || req.body.orderNumber < 0) {
        possibleErrors.push('orderNumber is required!');
    }
    if (possibleErrors.length > 0) {
        return handleErrorResponse(possibleErrors, res);
    }

    var buf = new Buffer(req.body.imageData.replace(/^data:image\/\w+;base64,/, ""), 'base64');
    var publicPrefixName = uid(20);
    var data = {
        Key: publicPrefixName,
        Body: buf,
        ContentEncoding: 'base64',
        ContentType: 'image/jpeg'
    };

    s3Bucket.putObject(data, function(err, data){
        if (err) {
            possibleErrors.push('Saving on bucket failed!');
        }

        if (possibleErrors.length > 0) {
            return handleErrorResponse(possibleErrors, res);
        }

        var newImage = new Image();
        newImage.description = req.body.description;
        newImage.tags = req.body.tags;
        newImage.orderNumber =  req.body.orderNumber;
        newImage.albumId =  req.body.albumId;
        newImage.isCover =  req.body.isCover;
        newImage.created_on = Date.now();
        newImage.imageUserCreator = req.user._id;
        newImage.imageUrl = publicPrefixName;
        newImage.type = 'Image';

        newImage.save(function (err) {
            if (err) {
                possibleErrors.push('Image not saved!');
            }

            if (possibleErrors.length == 0) {
                var output = newImage.outputMappedResult();
                output.status = 'created';
                return res.status(201).jsonp(output);
            } else {
                return handleErrorResponse(possibleErrors, res);
            }
        });
    });
});

image_routes.put('/:id', auth.authenticate(), function(req, res) {
    var possibleErrors = [];

    if (!req.params.id || req.params.id == 0) {
        possibleErrors.push('id is required!');
    }
    if (possibleErrors.length > 0) {
        return handleErrorResponse(possibleErrors, res);
    }

    Image.findOne({ _id: req.params.id}, function (err, image) {
        if (err || !image) {
            possibleErrors.push('Unable to find image for given id!');
        }

        Image.update({_id: req.params.id}, req.body, function (err) {
            if (err) {
                possibleErrors.push('Image not saved!');
            }

            if (possibleErrors.length == 0) {
                return res.status(200).jsonp({ status : 'updated'});
            } else {
                return handleErrorResponse(possibleErrors, res);
            }
        });
    });
});

image_routes.delete('/:id', auth.authenticate(), function(req, res) {
    var possibleErrors = [];
    if (!req.params.id || req.params.id == 0) {
        possibleErrors.push('id is required!');
    }
    if (possibleErrors.length > 0) {
        return handleErrorResponse(possibleErrors, res);
    }

    Image.findOne({ _id: req.params.id}, function (err, image) {
        if (err || !image) {
            possibleErrors.push('Unable to find image for given id!');
        }

        if (!image || !image.imageUserCreator.equals(req.user._id)) {
            possibleErrors.push('Not possible to delete this image!');
        }

        if (possibleErrors.length == 0) {
            var objectKeyName = image.imageUrl;

            image.remove(function (err) {
                if (err) {
                    possibleErrors.push('Unable to remove image');
                }

                if (possibleErrors.length == 0) {

                    s3Bucket.deleteObjects({
                        Bucket: config.S3BucketCredentials.bucketName,
                        Delete: {
                            Objects: [
                                { Key: objectKeyName }
                            ]
                        }
                    }, function(err, data) {});

                    return res.status(204).jsonp();
                } else {
                    return handleErrorResponse(possibleErrors, res);
                }
            });
        } else {
            return handleErrorResponse(possibleErrors, res);
        }
    });
});

module.exports = image_routes;