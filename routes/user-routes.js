var user_routes = require('express').Router();
var config = require('../config');
var User = require('../models/user');
var Album = require('../models/album');
var Activity = require('../models/activity');
var Image = require('../models/image');
var Weived = require('../models/weived');
var Weiving = require('../models/weiving');
var Followed = require('../models/followed');
var Following = require('../models/following');
var Order = require('../models/order');
var passport = require('passport');
var FacebookTokenStrategy = require('passport-facebook-token');
var GoogleTokenStrategy = require('passport-google-id-token');
var auth = require("../customAuth")();
var jwt = require('jsonwebtoken');
var request = require('request');
var handleErrorResponse = require('../helpers/errorHelper').handleErrorResponse;
var stripe = require("stripe")("sk_test_HUgR0fijvynpgp1EmKz6Z152");
var followingHelper = require('../helpers/followingHelper');
var notificationsHelper = require('../helpers/notificationsHelper');
var activityHelper = require('../helpers/activityHelper');

passport.serializeUser(function (user, done) {
    done(null, user);
});
passport.deserializeUser(function (user, done) {
    done(null, user);
});
passport.use(new FacebookTokenStrategy({
    clientID: "678076292372057",
    clientSecret: "2b2c4e1d50c8872768a8d38f9977c86e"
}, function (accessToken, refreshToken, profile, done) {
    User.findOrCreate({ facebookId: profile.id }, function (err, user) {
        var payload = {
            id: user._id
        };
        var token = jwt.sign(payload, config.secret);
        return done(null, token);
    })
}));
passport.use(new GoogleTokenStrategy({
    clientID: "751924306188-568gbra6qk657pfjhivmd5rkpdmoje84.apps.googleusercontent.com"
}, function (parsedToken, googleId, done) {
    User.findOrCreate({ googleId: googleId }, function (err, user) {
        var payload = {
            id: user._id
        };
        var token = jwt.sign(payload, config.secret);
        return done(null, token);
    })
}));

user_routes.post('/auth/facebook/token', passport.authenticate('facebook-token'), function (req, res) {
    // do something with req.user
    res.send(req.user);
});

user_routes.post('/auth/google/token', passport.authenticate('google-id-token'), function (req, res) {
    // do something with req.user
    res.send(req.user);
});

user_routes.post('/addFCMToken', auth.authenticate(), function (req, res) {
    if (!req.user.fcmList)
        req.user.fcmList = [];
    var fcm = {};
    fcm.fcmToken = req.body.fcmToken;
    var hasThisToken = false;
    req.user.fcmList.forEach(function (fcmElement) {
        if (fcmElement.fcmToken == req.body.fcmToken) {
            hasThisToken = true;
        }
    }, this);
    if (!hasThisToken) {
        req.user.fcmList.push(fcm);
        req.user.save(function (err, us) {
            if (err) {
                res.status(500).send(err)
            }
            res.send('ADDED FCM TOKEN.');
        });
    }
    else res.send('NOT ADDED FCM TOKEN');

});

user_routes.get('/info', auth.authenticate(), function (req, res) {
    res.send(req.user);
});

user_routes.put('/info', auth.authenticate(), function (req, res) {
    req.user.fullName = req.body.fullName || req.user.fullName;
    req.user.email = req.body.email || req.user.email;
    req.user.username = req.body.username || req.user.username;
    req.user.instagramHandle = req.body.instagramHandle || req.user.instagramHandle;
    //req.user.confirmed = req.body.confirmed || req.user.confirmed;
    //req.user.joined_on = req.body.joined_on || req.user.joined_on;
    //req.user.is_online = req.body.is_online || req.user.is_online;
    //req.user.finishedCreation = req.body.finishedCreation || req.user.finishedCreation;
    req.user.avatarUrl = req.body.avatarUrl || req.user.avatarUrl;
    req.user.heroUrl = req.body.heroUrl || req.user.heroUrl;
    req.user.gender = req.body.gender || req.user.gender;
    req.user.is_content_creator = req.body.is_content_creator || req.user.is_content_creator;
    req.user.description = req.body.description || req.user.description;
    req.user.birthYear = req.body.birthYear || req.user.birthYear;
    var newSubscriptionAdded = false;
    if (req.user.subscription_plan != req.body.subscription_plan && req.body.subscription_plan) {
        req.user.subscription_plan = req.body.subscription_plan;
        newSubscriptionAdded = true;
    }

    req.user.save(function (err, us) {
        if (err) {
            res.status(500).send(err)
        }
        if (newSubscriptionAdded) {
            var plan = stripe.plans.create({
                name: req.user.subscription_plan + " plan",
                id: "plan-" + req.user.subscription_plan,
                interval: "month",
                currency: "usd",
                amount: req.user.subscription_plan,
            }, {
                    stripe_account: req.user.content_creator_stripe_id
                }
                , function (err1, plan) {
                    if (err1) {
                        res.status(500).send(err)
                    }
                    res.send(us);
                });
        }
        else
            res.send(us);
    });
});

user_routes.post('/', function (req, res) {
    return res.send('Not implemented!');
});

user_routes.post('/connectWithStripe', auth.authenticate(), function (req, res) {
    var code = req.body.code;
    var accessTokenUrl = "https://connect.stripe.com/oauth/token";
    var accessTokenRequest = {};
    accessTokenRequest.client_secret = "sk_test_HUgR0fijvynpgp1EmKz6Z152";
    accessTokenRequest.code = code;
    accessTokenRequest.grant_type = "authorization_code";
    request.post(accessTokenUrl, { form: accessTokenRequest }, function (err, response, body) {
        if (response.statusCode !== 200) {
            return res.status(response.statusCode).send({ message: body.error_description });
        }
        var bodyObj = JSON.parse(body);
        req.user.content_creator_stripe_id = bodyObj.stripe_user_id;
        req.user.finishedCreation = true;
        req.user.save(function (err, us) {
            if (err) {
                res.status(500).send(err)
            }
            res.send({
                status: "linked"
            });
        });

    });
});

//Weive person with id that you send in parameter
//You have to be authenticated to use this service
user_routes.post('/:id/weive', auth.authenticate(), function (req, res) {
    var possibleErrors = [];
    var givenUser = {};
    if (!req.user._id.equals(req.params.id)) {
        User.findOne({ _id: req.params.id }, function (err, user) {
            if (err || !user) {
                possibleErrors.push('Unable to find user for given id!');
            }

            if (err) {
                res.status(500).send(err)
            }
            else givenUser = user;


            Weived.findOrCreate(req.user, function (err, weived) {
                var wuser = weived.users.find(function (wus) {
                    return wus.userId.equals(givenUser._id);
                });
                if (!wuser)
                    weived.users.push({ userId: givenUser._id });

                weived.save(function (err, us) {
                    if (err) {
                        res.status(500).send(err)
                    }
                });


            });
            Weiving.findOrCreate(givenUser, function (err, weiving) {
                var wuser = weiving.users.find(function (wus) {
                    return wus.userId.equals(req.user._id);
                });
                if (!wuser)
                    weiving.users.push({ userId: req.user._id });

                weiving.save(function (err, us) {
                    if (err) {
                        res.status(500).send(err)
                    }
                });

            });
        });
    }
    res.send("WEIVED USER");

});

//Unweive person with id that you send in parameter
//You have to be authenticated to use this service
user_routes.post('/:id/unweive', auth.authenticate(), function (req, res) {
    var possibleErrors = [];
    var givenUser = {};

    User.findOne({ _id: req.params.id }, function (err, user) {
        if (err || !user) {
            possibleErrors.push('Unable to find user for given id!');
        }

        if (err) {
            res.status(500).send(err)
        }
        else givenUser = user;


        Weived.findOneAndUpdate({ userId: req.user._id }, { $pull: { users: { userId: givenUser._id } } }, function (err, data) {
            //console.log(err, data);
        });
        Weiving.findOneAndUpdate({ userId: givenUser._id }, { $pull: { users: { userId: req.user._id } } }, function (err, data) {
            //console.log(err, data);
        });
    });

    res.send("UNWEIVED USER");
});

//Get all users that you weived to
//You have to be authenticated to use this service
user_routes.get('/weived', auth.authenticate(), function (req, res) {
    var possibleErrors = [];
    Weived.findOne({ userId: req.user._id }).populate('users.userId').exec(function (err, user) {
        if (err) {
            possibleErrors.push('Something has gone wrong');
        }
        if (!user) {
            return res.status(200).jsonp({
                matches: []
            });
        }
        else if (!user.users) {
            user.users = [];
        }
        if (possibleErrors.length > 0) {
            return handleErrorResponse(possibleErrors, res);
        } else {
            var results = [];
            //fix dont want to change response
            user.users.forEach(function (el) {
                results.push(el.userId);
            }, this);


            return res.status(200).jsonp({
                matches: results
            });
        }
    });
});

user_routes.get('/bought', auth.authenticate(), followingHelper.getFollowingUsers, function (req, res) {
    var possibleErrors = [];

    User.findOne({ _id: req.user._id }).populate({
        path: 'bought.album',
        model: 'Album',
        populate: {
            path: 'albumUserCreator',
            model: 'User'
        }
    }).exec(function (err, user) {

        if (err || !user || user.deleted) {
            possibleErrors.push('Unable to find user for given id');
        }

        if (possibleErrors.length > 0) {
            return handleErrorResponse(possibleErrors, res);
        } else {



            var allBought = user.bought.toObject();
            var albums = allBought.map(function (x) { return x.album; });
            if (err || !albums) {
                albums = [];
            }
            var albumIds = albums.map(function (x) { return x._id; });
            Image.find({ albumId: { $in: albumIds } }, function (errI, images) {
                if (err || !images) {
                    images = [];
                }
                allBought.forEach(function (boughtItem) {
                    var outputImages = [];
                    if (req.following != null) {
                        var isfollowing = req.following.some(function (fuser) {
                            return fuser.userId.equals(boughtItem.album.albumUserCreator._id);
                        });
                        boughtItem.album.albumUserCreator.following = isfollowing;
                    }
                    images.forEach(function (image) {
                        if (image.albumId == boughtItem.album._id.toString()) {
                            outputImages.push(image.outputMappedResult());
                        }
                        boughtItem.album.images = outputImages;
                    }, this);
                }, this);
                return res.send(allBought);
            });




        }
    });

});

user_routes.get('/search', function (req, res) {

    var possibleErrors = [];
    var skip = 0;
    var limit = 1000;
    var query = "";
    if (req.query.size && Number(req.query.size) > 0) {
        limit = Number(req.query.size);
    }
    if (req.query.page && Number(req.query.page) >= 0) {
        skip = Number(req.query.page) * limit;
    }
    if (!req.query.search) {
        possibleErrors.push('search query string parameter is required!');
    }

    if (possibleErrors.length > 0) {
        return handleErrorResponse(possibleErrors, res);
    }

    User.count({
        $and: [{ $text: { $search: req.query.search } },
        { confirmed: true }, { is_content_creator: true }, { finishedCreation: true }, { deleted: false }
        ]
    }, function (err, count) {
        User.find({
            $and: [{ $text: { $search: req.query.search } },
            { confirmed: true }, { is_content_creator: true }, { finishedCreation: true }, { deleted: false }
            ]
        },
            { score: { $meta: 'textScore' } })
            .sort({ score: { $meta: 'textScore' } })
            .skip(skip)
            .limit(limit)
            .exec(function (err, users) {

                var users_output = users.map(function (x) { return x.outputSearchResult(); });

                if (possibleErrors.length == 0) {
                    return res.status(200).jsonp({
                        page: skip / limit,
                        size: limit,
                        total: count,
                        users: users_output || []
                    });
                } else {
                    return handleErrorResponse(possibleErrors, res);
                }
            });
    })
});

user_routes.get('/profile/:id', auth.authenticate(), followingHelper.getFollowingUsers, function (req, res) {
    var possibleErrors = [];

    User.findOne({ _id: req.params.id }, function (err, user) {
        if (err || !user || user.deleted) {
            possibleErrors.push('Unable to find user for given id');
        } else if (!user.confirmed || !user.finishedCreation) {
            possibleErrors.push('User is not approved seller!');
        }

        if (possibleErrors.length > 0) {
            return handleErrorResponse(possibleErrors, res);
        } else {
            Album.find({ albumUserCreator: user._id, stage: 'published' }).populate('albumUserCreator').populate({ path: 'content', populate: { path: 'image' } }).sort({ created_on: -1 }).exec(function (err, albums) {
                if (err || !albums) {
                    albums = [];
                }
                //isFollowing user
                var tempUser = user.outputSellerPublicMappedResult(req.user, albums);
                var isFollowing = false;

                req.following.forEach(function (flwElement) {
                    if (flwElement.userId.equals(req.params.id))
                        isFollowing = true;
                }, this);
                tempUser.following = isFollowing;
                return res.status(200).jsonp(tempUser);

            });
        }
    });
});

user_routes.post('/subscribe/:id', auth.authenticate(), followingHelper.getFollowingUsers, function (req, res) {
    var possibleErrors = [];

    User.findOne({ _id: req.params.id }, function (err, user) {
        if (err || !user || user.deleted) {
            possibleErrors.push('Unable to find user for given id');
        } else if (!user.confirmed || !user.finishedCreation) {
            possibleErrors.push('User is not approved seller!');
        }

        if (possibleErrors.length > 0) {
            return handleErrorResponse(possibleErrors, res);
        }
        var stripeToken = req.body.stripe_token;

        var customer = stripe.customers.create({
            email: req.user.email,
            source: stripeToken
        }, function (err, customer) {


            stripe.tokens.create({
                customer: customer.id,
            }, {
                    stripe_account: user.content_creator_stripe_id,
                }).then(function (tokenNew) {

                    var connectedCustomer = stripe.customers.create({
                        description: "New customer " + req.user.email,
                        source: tokenNew.id
                    }, {
                            stripe_account: user.content_creator_stripe_id
                        }
                        , function (err, conCustomer) {
                            stripe.subscriptions.create({
                                customer: conCustomer.id,
                                plan: "plan-" + user.subscription_plan,
                                application_fee_percent: 10
                            }, {
                                    stripe_account: user.content_creator_stripe_id,
                                }, function (err1, subscription) {

                                    req.user.subscribedTo.push({
                                        user: req.params.id,
                                        date: Date.now()
                                    });

                                    req.user.save(function (errSaveUser) {
                                        if (errSaveUser) {
                                            possibleErrors.push('User not saved!');
                                        }
                                        user.save(function (errSaveOtherUser) {
                                            if (errSaveOtherUser) {
                                                possibleErrors.push('Album not saved!');
                                            }

                                            if (possibleErrors.length == 0) {


                                                var act = new Activity();
                                                act.message = "You have subscribed to " + user.username;
                                                act.activityType = "SUBSCRIBED";
                                                act.subject = {}; act.objecta = {};
                                                act.subject.user = req.user;
                                                act.objecta.user = user;
                                                var subUsers = []; subUsers.push(req.user);
                                                activityHelper.pushNewActivity(act, subUsers, function (resA) {
                                                    // you have to fix this
                                                    var isFollowing = false;

                                                    req.following.forEach(function (flwElement) {
                                                        if (flwElement.userId.equals(user._id))
                                                            isFollowing = true;
                                                    }, this);

                                                    if (!isFollowing)
                                                        followingHelper.followUser(req.user, user);

                                                    notificationsHelper.sendNotifications("You have a new subscriber", req.user.username + "has subscribed to you.", user.fcmList);
                                                    res.status(200).jsonp({
                                                        subscribed: true
                                                    });
                                                })
                                            } else {
                                                return handleErrorResponse(possibleErrors, res);
                                            }
                                        });
                                    });

                                });
                        });
                });




        });



    });
});


user_routes.post('/v2/subscribe/:id', auth.authenticate(), followingHelper.getFollowingUsers, function (req, res) {
    var possibleErrors = [];

    User.findOne({ _id: req.params.id }, function (err, user) {
        if (err || !user || user.deleted) {
            possibleErrors.push('Unable to find user for given id');
        } else if (!user.confirmed || !user.finishedCreation) {
            possibleErrors.push('User is not approved seller!');
        }

        if (possibleErrors.length > 0) {
            return handleErrorResponse(possibleErrors, res);
        }

        var subscriptionOrder = new Order();
        subscriptionOrder.user = req.params.id;
        subscriptionOrder.transaction = req.body.transaction.id;
        subscriptionOrder.date = req.body.transaction.date;

        subscriptionOrder.save(function (errSubscriptionOrder,subOrder) {


            req.user.subscribedTo.push({
                user: req.params.id,
                transaction: subOrder._id,
                active:true,
                receipt: req.body.transaction.receipt,
                date: req.body.transaction.date
            });

            user.subscribed.push({
                user: req.user._id,
                date: req.body.transaction.date
            })

            req.user.save(function (errSaveUser) {
                if (errSaveUser) {
                    possibleErrors.push('User not saved!');
                }
                user.save(function (errSaveOtherUser) {
                    if (errSaveOtherUser) {
                        possibleErrors.push('Album not saved!');
                    }

                    if (possibleErrors.length == 0) {


                        var act = new Activity();
                        act.message = "You have subscribed to " + user.username;
                        act.activityType = "SUBSCRIBED";
                        act.subject = {}; act.objecta = {};
                        act.subject.user = req.user;
                        act.objecta.user = user;
                        var subUsers = []; subUsers.push(req.user);
                        activityHelper.pushNewActivity(act, subUsers, function (resA) {
                            // you have to fix this
                            var isFollowing = false;

                            req.following.forEach(function (flwElement) {
                                if (flwElement.userId.equals(user._id))
                                    isFollowing = true;
                            }, this);

                            if (!isFollowing)
                                followingHelper.followUser(req.user, user);

                            notificationsHelper.sendNotifications("You have a new subscriber", req.user.username + "has subscribed to you.", user.fcmList);
                            res.status(200).jsonp({
                                subscribed: true
                            });
                        })
                    } else {
                        return handleErrorResponse(possibleErrors, res);
                    }
                });
            });
        });
    });
});





//FOLLOWING FOLLOWED
user_routes.post('/:id/follow', auth.authenticate(), function (req, res) {
    var possibleErrors = [];
    var givenUser = {};
    if (!req.user._id.equals(req.params.id)) {
        User.findOne({ _id: req.params.id }, function (err, user) {
            if (err || !user) {
                possibleErrors.push('Unable to find user for given id!');
            }

            if (err) {
                res.status(500).send(err)
            }
            else givenUser = user;


            Followed.findOrCreate(req.user, function (err, followed) {
                var wuser = followed.users.find(function (wus) {
                    return wus.userId.equals(givenUser._id);
                });
                if (!wuser)
                    followed.users.push({ userId: givenUser._id });

                followed.save(function (err, us) {
                    if (err) {
                        res.status(500).send(err)

                    }


                });


            });
            Following.findOrCreate(givenUser, function (err, following) {
                var wuser = following.users.find(function (wus) {
                    return wus.userId.equals(req.user._id);
                });
                if (!wuser) {
                    following.users.push({ userId: req.user._id });
                    var act = new Activity();
                    act.message = req.user.username + " has started following you"; act.activityType = "FOLLOWING";
                    act.subject = {}; act.objecta = {}; act.subject.user = req.user; act.objecta.user = givenUser;
                    var subUsers = []; subUsers.push(givenUser);
                    activityHelper.pushNewActivity(act, subUsers, function (resA) {
                        console.log(resA);
                    });
                    var act1 = new Activity();
                    act1.message = "You have started following " + givenUser.username; act1.activityType = "FOLLOWING";
                    act1.subject = {}; act1.objecta = {}; act1.subject.user = req.user; act1.objecta.user = givenUser;
                    var subUsers1 = []; subUsers1.push(req.user);
                    activityHelper.pushNewActivity(act1, subUsers1, function (resA) {
                        console.log(resA);
                    });
                    notificationsHelper.sendNotifications("You have new follower", req.user.username + " has started following you.", givenUser.fcmList);
                }

                following.save(function (err, us) {
                    if (err) {
                        res.status(500).send(err)

                    }
                });

            });
        });
    }
    res.send("FOLLOWED USER");

});

//Unweive person with id that you send in parameter
//You have to be authenticated to use this service
user_routes.post('/:id/unfollow', auth.authenticate(), function (req, res) {
    var possibleErrors = [];
    var givenUser = {};

    User.findOne({ _id: req.params.id }, function (err, user) {
        if (err || !user) {
            possibleErrors.push('Unable to find user for given id!');
        }

        if (err) {
            res.status(500).send(err)
        }
        else givenUser = user;


        Followed.findOneAndUpdate({ userId: req.user._id }, { $pull: { users: { userId: givenUser._id } } }, function (err, data) {
            //console.log(err, data);
        });
        Following.findOneAndUpdate({ userId: givenUser._id }, { $pull: { users: { userId: req.user._id } } }, function (err, data) {
            //console.log(err, data);
        });
    });

    res.send("UNFOLLOWED USER");
});
//TODO : Add skip/limit and optimize a code a lil bit
user_routes.get('/followingAlbums', auth.authenticate(), followingHelper.getFollowingUsers, function (req, res) {
    var possibleErrors = [];
    Followed.findOne({ userId: req.user._id }).populate('users.userId').exec(function (err, user) {
        if (err) {
            possibleErrors.push('Something has gone wrong');
        }
        if (!user) {
            return res.status(200).jsonp({
                matches: []
            });
        }
        else if (!user.users) {
            user.users = [];
        }
        if (possibleErrors.length > 0) {
            return handleErrorResponse(possibleErrors, res);
        } else {
            var results = [];
            var resultAlbums = [];

            //fix dont want to change response
            user.users.forEach(function (el) {
                results.push(el.userId);
            }, this);
            var userIds = results.map(function (x) { return x._id; });

            Album.find({ albumUserCreator: { $in: userIds }, isPublished: true }, function (errI, albums) {
                var albumIds = albums.map(function (x) { return x._id; });
                Image.find({ albumId: { $in: albumIds } }, function (errI, images) {

                    var albums_output = albums.map(function (x) { return x.outputMappedResultWithCreator(images, results, req.following); });

                    if (possibleErrors.length == 0) {
                        return res.status(200).jsonp({

                            albums: albums_output
                        });
                    } else {
                        return handleErrorResponse(possibleErrors, res);
                    }
                });
            });


        }
    });
});

user_routes.get('/v2/followingAlbums', auth.authenticate(), followingHelper.getFollowingUsers, function (req, res) {
    var possibleErrors = [];
    Followed.findOne({ userId: req.user._id }).populate('users.userId').exec(function (err, user) {
        if (err) {
            possibleErrors.push('Something has gone wrong');
        }
        if (!user) {
            return res.status(200).jsonp({
                matches: []
            });
        }
        else if (!user.users) {
            user.users = [];
        }
        if (possibleErrors.length > 0) {
            return handleErrorResponse(possibleErrors, res);
        } else {
            var results = [];
            var resultAlbums = [];

            //fix dont want to change response
            user.users.forEach(function (el) {
                results.push(el.userId);
            }, this);
            var userIds = results.map(function (x) { return x._id; });

            Album.find({ albumUserCreator: { $in: userIds }, stage: 'published' }).populate({ path: 'content', populate: { path: 'image' } }).populate('albumUserCreator').sort('-created_on').exec(function (errI, albums) {
                if (possibleErrors.length == 0) {
                    return res.status(200).jsonp({
                        albums: albums
                    });
                } else {
                    return handleErrorResponse(possibleErrors, res);
                }

            });


        }
    });
});


user_routes.get('/creators',
    auth.authenticate(),
    function (req, res) {
        var subscribedUsers = [];
        var boughtUsers = [];
        var followedUsers = [];
        User.findOne({ _id: req.user._id }).
            populate([{ path: 'subscribedTo.user', model: 'User' }, {
                path: 'bought.album',
                model: 'Album',
                populate: {
                    path: 'albumUserCreator',
                    model: 'User'
                }
            }]).exec(function (err, user) {

                subscribedUsers = user.subscribedTo;
                user.bought.forEach(function (element) {
                    boughtUsers.push(element.album.albumUserCreator);
                }, this);
                Followed.findOne({ userId: req.user._id }).populate('users.userId').exec(function (err, user) {
                    user.users.forEach(function (el) {
                        followedUsers.push(el.userId);
                    }, this);

                    var resultList = new Map();
                    subscribedUsers.forEach(function (element) {
                        element = element.toObject();
                        element.user.relation = "SUBSCRIBED";
                        resultList.set(element.user._id.toString(), element.user);
                    }, this);
                    boughtUsers.forEach(function (element) {
                        if (!resultList.has(element._id.toString())) {
                            element = element.toJSON();
                            element.relation = "SAVED";
                            resultList.set(element._id.toString(), element);
                        }
                    }, this);
                    followedUsers.forEach(function (element) {
                        if (!resultList.has(element._id.toString())) {
                            element = element.toObject();
                            element.relation = "FOLLOWED";
                            resultList.set(element._id.toString(), element);
                        }
                    }, this);

                    var flattenedList = [];
                    for (var [key, value] of resultList) {
                        flattenedList.push(value);
                    }

                    res.status(200).jsonp(flattenedList);


                });


            });
    });

user_routes.get('/mycreators',
    auth.authenticate(),
    function (req, res) {
        var subscribedUsers = [];
        var boughtUsers = [];
        var followedUsers = [];
        User.findOne({ _id: req.user._id }).
            populate([{ path: 'subscribed.user', model: 'User' }, {
                path: 'bought.album',
                model: 'Album',
                populate: {
                    path: 'albumUserCreator',
                    model: 'User'
                }
            }]).exec(function (err, user) {

                subscribedUsers = user.subscribed;
                // user.bought.forEach(function (element) {
                //     boughtUsers.push(element.album.albumUserCreator);
                // }, this);
                Following.findOne({ userId: req.user._id }).populate('users.userId').exec(function (err, user) {
                    if (user) {
                        user.users.forEach(function (el) {
                            followedUsers.push(el.userId);
                        }, this);
                    }

                    var resultList = new Map();
                    subscribedUsers.forEach(function (element) {
                        element = element.toObject();
                        element.user.relation = "SUBSCRIBED";
                        resultList.set(element.user._id.toString(), element.user);
                    }, this);
                    boughtUsers.forEach(function (element) {
                        if (!resultList.has(element._id.toString())) {
                            element = element.toJSON();
                            element.relation = "SAVED";
                            resultList.set(element._id.toString(), element);
                        }
                    }, this);
                    followedUsers.forEach(function (element) {
                        if (!resultList.has(element._id.toString())) {
                            element = element.toObject();
                            element.relation = "FOLLOWED";
                            resultList.set(element._id.toString(), element);
                        }
                    }, this);

                    var flattenedList = [];
                    for (var [key, value] of resultList) {
                        flattenedList.push(value);
                    }

                    res.status(200).jsonp(flattenedList);


                });


            });
    });


user_routes.get('/activity', auth.authenticate(), function (req, res) {
    var possibleErrors = [];
    var givenUser = {};
    User.findOne({ _id: req.user._id }).populate({ path: 'activities', model: 'Activity', populate: [{ path: 'objecta.user', model: 'User' }, { path: 'subject.user', model: 'User' }, { path: 'objecta.album', model: 'Album' }, { path: 'subject.album', model: 'Album' }] }).exec(function (err, user) {
        res.status(200).jsonp(user.activities.reverse());
    });
});

module.exports = user_routes;
