var admin = require("firebase-admin");
var utils = require("./utils");

var serviceAccount = require("../serviceNotificationKey.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://weive-2aa12.firebaseio.com"
});

var notificationsHelper = {};

notificationsHelper.sendNotifications = function (notificationTitle,notificationText, fcmTokens) {
    if(fcmTokens)
    {
        fcmTokens=checkForEmptyToken(fcmTokens);
    var payload = {
        notification: {
            title: notificationTitle,
            body: notificationText
        }
    }
    admin.messaging().sendToDevice(utils.convertToIds(fcmTokens,'fcmToken'), payload)
        .then(function (response) {
            console.log("Successfully sent message:", response);
        })
        .catch(function (error) {
            console.log("Error sending message:", error);
        });
    }
}

var checkForEmptyToken=function(tokens)
{
    var nonEmptyTokens=[];
    tokens.forEach(function(element) {
        if(element.fcmToken!="")
        {
            nonEmptyTokens.push(element);
        }
    }, this);
    return nonEmptyTokens;
}


module.exports = notificationsHelper;