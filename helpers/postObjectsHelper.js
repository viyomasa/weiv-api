var postObjectsHelper = {};
var TextObject=require('../models/postobjects/textObject');
var ImageObject=require('../models/postobjects/imageObject');
var BasePostObject=require('../models/postobjects/basePostObject');

postObjectsHelper.getPostObject=function(po,cb)
{
    BasePostObject.findOne({ _id:po._id }, function (err, bpo) {
            cb(bpo);
    });
}

postObjectsHelper.generatePostObject=function(po,cb)
{
    var gpo={};
    switch(po.type)
    {
        case "text":
            gpo=new TextObject();
            gpo.orderNum=po.orderNum;
            gpo.postType=po.type;
            gpo.text=po.text;
            gpo.font=po.font;

            break;
        case "image":
            gpo=new ImageObject();
            gpo.orderNum=po.orderNum;
            gpo.postType=po.type;
            gpo.dimension=po.dimension;
            gpo.image=po.image;
            break;
    }
    gpo.save(function (err,spo) {
        cb(spo._id);
    });
}

module.exports = postObjectsHelper;