/**
 * Created by nemanja on 6/3/17.
 */
var jwt = require('jsonwebtoken');
var config  = require('../config');

var getUserFromToken = function(token, callback) {
    if (token) {
        jwt.verify(token, config.secret, function (err, decoded) {
            if (err) {
                console.trace('GetUserFromToken error');
                console.log(err);
                callback(err, null);
            } else {
                callback(null, decoded);
            }
        });

    } else {
        callback(new Error('Missing token', null));
    }
};

exports.getUserFromToken = getUserFromToken;