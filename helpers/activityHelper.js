var activityHelper = {};
var Activity = require('../models/activity');
var User = require('../models/user');

activityHelper.pushNewActivity = function (activity,subscribedUsers,cb) {
   activity.save(function (errActivity,savedActivity) {
        subscribedUsers.forEach(function(element) {
            User.update({ _id: {$in:subscribedUsers} }, { $push: {"activities": savedActivity._id}}, function(err1)
            {
                cb(savedActivity);
            });
            
        }, this);
   });

}


module.exports = activityHelper;