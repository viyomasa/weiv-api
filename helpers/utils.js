var utils = {};

utils.convertToIds = function (array) {
   return array.map(function (x) { return x._id; });
}

utils.convertToIds = function (array,customParameter) {
   return array.map(function (x) { return x[customParameter]; });
}

module.exports = utils;