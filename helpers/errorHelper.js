/**
 * Created by nemanja on 4/5/17.
 */

var handleErrorResponse = function(errorCodes, res) {
    return res.status(403).jsonp({
        status: 'fail',
        errors: errorCodes
    });
};

exports.handleErrorResponse = handleErrorResponse;