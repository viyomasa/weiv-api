var imageHelper = {};
var User = require('../models/user');
var Image = require('../models/image');

imageHelper.joinImagesCreatorsFollowing = function (albums,following,cb) {

    var albumCreatorIds = albums.map(function (x) { return x.albumUserCreator; });

    User.find({ _id: albumCreatorIds }, function (err, creators) {

        var albumIds = albums.map(function (x) { return x._id; });
        Image.find({ albumId: { $in: albumIds } }, function (errI, images) {

            var albums_output = albums.map(function (x) { return x.outputMappedResultWithCreator(images, creators, following); });
            cb(albums_output);
        });
    });
}


module.exports = imageHelper;