
var followingHelper = {};
var Followed = require('../models/followed');
var Following = require('../models/following');

followingHelper.getFollowingUsers = function (req, res, next) {
    Followed.findOne({ userId: req.user._id }, function (err, user) {
        if (user)
            req.following = user.users;
        else
            req.following = [];
        next();
    });
}
followingHelper.getFollowingUsersWithDetails = function (req, res, next) {
    Followed.findOne({ userId: req.user._id }).populate('users.userId').exec(function (err, user) {
        if (user)
            req.following = user.users.map(a => a.userId);
        else
            req.following = [];
        next();
    });
}
followingHelper.followUser = function (user1, user2) {

    Followed.findOrCreate(user1, function (err, followed) {
        var wuser = followed.users.find(function (wus) {
            return wus.userId.equals(user2._id);
        });
        if (!wuser)
            followed.users.push({ userId: user2._id });

        followed.save(function (err, us) {
            if (err) {
                res.status(500).send(err)
            }
            Following.findOrCreate(user2, function (err, following) {
                var wuser = following.users.find(function (wus) {
                    return wus.userId.equals(user1._id);
                });
                if (!wuser) {
                    following.users.push({ userId: user1._id });
                    var act = new Activity();
                    act.message = user1.username + " has started following you"; act.activityType = "FOLLOWING";
                    act.subject = {}; act.objecta = {}; act.subject.user = user1; act.objecta.user = user2;
                    var subUsers = []; subUsers.push(user2);
                    activityHelper.pushNewActivity(act, subUsers, function (resA) {
                        console.log(resA);
                    });
                    var act1 = new Activity();
                    act1.message = "You have started following " + user2.username; act1.activityType = "FOLLOWING";
                    act1.subject = {}; act1.objecta = {}; act1.subject.user = user1; act1.objecta.user = user2;
                    var subUsers1 = []; subUsers1.push(user1);
                    activityHelper.pushNewActivity(act1, subUsers1, function (resA) {
                        console.log(resA);
                    });
                    notificationsHelper.sendNotifications("You have new follower", user1.username + " has started following you.", user2.fcmList);
                }

                following.save(function (err, us) {
                    if (err) {
                        res.status(500).send(err)
                    }
                    cb();
                });

            });

        });


    });

}

module.exports = followingHelper;